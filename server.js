var express = require('express'),
  app = express(),
  port = process.env.PORT || 3009;

var path = require('path');
app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('Iniciado Polymer desde Node para ProyectoTechUF en el puerto: ' + port);

app.get("/",function (req, res){
  res.sendFile("index.html",{root:'.'});
})
